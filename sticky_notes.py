from flask import Flask, request, jsonify
from flask_cors import CORS

#from werkzeug.utils import secure_filename

from pony.orm import *

import os
import json
import random

db = Database()

class User(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str)
    password = Required(str)
    fontSize = Required(int)
    palette = Required(int)
    defaultImage = Required(int)
    image = Required(int)

class Note(db.Entity):
    id = PrimaryKey(int, auto=True)
    x = Required(int)
    y = Required(int)
    z = Required(int)
    width = Required(int)
    height = Required(int)
    title = Optional(str)
    text = Optional(str)
    color = Required(str) 
    hidden = Required(bool)
    fontSize = Required(int)
    userId = Required(int)

class ArchivedNote(db.Entity):
    orderId = PrimaryKey(int, auto=True)
    id = Required(int)
    x = Required(int)
    y = Required(int)
    z = Required(int)
    width = Required(int)
    height = Required(int)
    title = Optional(str) 
    text = Optional(str) 
    color = Required(str) 
    hidden = Required(bool)
    fontSize = Required(int)
    userId = Required(int)

class DeletedNote(db.Entity):
    orderId = PrimaryKey(int, auto=True)
    id = Required(int)
    x = Required(int)
    y = Required(int)
    z = Required(int)
    width = Required(int)
    height = Required(int)
    title = Optional(str) 
    text = Optional(str) 
    color = Required(str) 
    hidden = Required(bool)
    fontSize = Required(int)
    userId = Required(int)

db.bind(provider='sqlite', filename='sticky_notes.sqlite', create_db=True)
db.generate_mapping(create_tables=True)


UPLOAD_FOLDER = 'src/assets'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
CORS(app)

activeUsers = {}

def createSessionId():
    return random.randint(1, 99999999)

def isSesssionIdOK(request, userId):
    sessionId = int(request.headers['Sessionid'])
    if activeUsers[userId] != sessionId: return False
    return True

@app.get("/api/notes")
def get_notes():
    userIdArg = int(request.args.get('userId'))
    if (not isSesssionIdOK(request, userIdArg)): return []

    with db_session:
        resp = select(note for note in Note if note.userId==userIdArg)
        result = {'data': [n.to_dict() for n in resp]}
    return jsonify(result['data'])

@app.get("/api/archived")
def get_archived():
    userIdArg = int(request.args.get('userId'))
    if (not isSesssionIdOK(request, userIdArg)): return []

    result = []
    with db_session:
        resp = select(note for note in ArchivedNote if note.userId==userIdArg)
        for n in resp:
            d = n.to_dict()
            del d['orderId']
            result.append(d)
    return jsonify(result)

@app.get("/api/deleted")
def get_deleted():
    userIdArg = int(request.args.get('userId'))
    if (not isSesssionIdOK(request, userIdArg)): return []

    result = []
    with db_session:
        resp = select(note for note in DeletedNote if note.userId==userIdArg)
        for n in resp:
            d = n.to_dict()
            del d['orderId']
            result.append(d)
    return jsonify(result)

@app.post("/api/notes")
def add_note():
    data = request.get_data()
    note = json.loads(data)
    del note['id']
    if (not isSesssionIdOK(request, note['userId'])): return {}
    with db_session:
        newNote = Note(**note)#json.loads(data, object_hook=lambda d: Note(**d))
        commit()
        return {'id': newNote.id}
    return {} # error ?

@app.put("/api/notes")
def update_note():
    data = request.get_data()
    updatedNote = json.loads(data)
    if (not isSesssionIdOK(request, updatedNote['userId'])): return {}

    with db_session:
        resp = select(note for note in Note if note.id==updatedNote['id'])
        for note in resp:
            note.set(**updatedNote)
            commit()
    return {}

@app.post("/api/deleted")
def add_note_deleted():
    data = request.get_data()
    note = json.loads(data)
    if (not isSesssionIdOK(request, note['userId'])): return {}

    with db_session:
        newNote = json.loads(data, object_hook=lambda d: DeletedNote(**d))
        commit()
    return {}

@app.post("/api/archived")
def add_note_archived():
    data = request.get_data()
    note = json.loads(data)
    if (not isSesssionIdOK(request, note['userId'])): return {}

    with db_session:
        newNote = json.loads(data, object_hook=lambda d: ArchivedNote(**d))
        commit()
    return {}

@app.get("/api/settings")
def get_settings():
    userIdArg = int(request.args.get('userId'))
    if (not isSesssionIdOK(request, userIdArg)): return {}
    with db_session:
        resp = select(user for user in User if user.id==userIdArg)
        for u in resp:
            return {'userId': u.id, 'palette': u.palette, 'fontSize': u.fontSize, 'image': u.image, 'defaultImage': u.defaultImage}
    return {}  # ?

@app.post("/api/settings")
def update_settings():
    data = request.get_data()
    settings = json.loads(data)
    if (not isSesssionIdOK(request, settings['userId'])): return {}

    with db_session:
        resp = select(user for user in User if user.id==settings['userId'])
        for u in resp:
            u.set(palette=settings['palette'], fontSize=settings['fontSize'], image=settings['image'], defaultImage=settings['defaultImage'])
            commit()
            return {}
    return {}

@app.post("/api/password")
def update_password():
    data = request.get_data()
    settings = json.loads(data)
    if (not isSesssionIdOK(request, settings['userId'])): return {}

    with db_session:
        resp = select(user for user in User if user.id==settings['userId'])
        for u in resp:
            u.set(password=settings['password'])
            commit()
            activeUsers[u.id] = createSessionId()
            return {'login': True, 'userId': u.id, 'sessionId': activeUsers[u.id]}
    return {}

@app.delete("/api/notes/delete")
def delete_note():
    noteId = int(request.args['id'])

    with db_session:
        resp = select(note for note in Note if note.id==noteId)
        for note in resp:
            if (not isSesssionIdOK(request, note.userId)): return {}
            note.delete()
        commit()
    return {}

@app.delete("/api/deleted/delete")
def delete_deleted_note():
    noteId = int(request.args['id'])
    with db_session:
        resp = select(note for note in DeletedNote if note.id==noteId)
        for note in resp:
            if (not isSesssionIdOK(request, note.userId)): return {}
            note.delete()
        commit()
    return {}

@app.delete("/api/archived/delete")
def delete_archived_note():
    noteId = int(request.args['id'])
    with db_session:
        resp = select(note for note in ArchivedNote if note.id==noteId)
        for note in resp:
            if (not isSesssionIdOK(request, note.userId)): return {}
            note.delete()
        commit()
    return {}

@app.post("/api/login")
def login():
    data = request.get_data()
    userData = json.loads(data)
    with db_session:
        resp = select(user for user in User if user.name==userData['name'] and user.password==userData['password'])
        for u in resp:
            activeUsers[u.id] = createSessionId()
            return {'login': True, 'userId': u.id, 'sessionId': activeUsers[u.id]}
    return {'login': False, 'error': 'User name or password is incorrect'}

@app.post("/api/create")
def createUser():
    data = request.get_data()
    userData = json.loads(data)
    with db_session:
        resp = select(user for user in User if user.name==userData['name'])
        for u in resp:
            return {'login': False, 'error': 'User name already exists'}
        newUser = User(name=userData['name'], password=userData['password'], fontSize=1, palette=0, image=0, defaultImage=0)
        commit()
        activeUsers[newUser.id] = createSessionId()
        return {'login': True, 'userId': newUser.id, 'sessionId': activeUsers[newUser.id]}

    return {'login': False}

def deleteNotesForUser(notes):
    for note in notes:
        note.delete()

@app.delete("/api/users/delete")
def delete_user():
    userId = int(request.args['id'])
    print ('delete user ' + str(userId))
    if (not isSesssionIdOK(request, userId)): return {}

    with db_session:
        resp = select(user for user in User if user.id==userId)
        for user in resp:
            user.delete()
            if userId in activeUsers:
                del activeUsers[userId]

        deleteNotesForUser(select(note for note in Note if note.userId==userId))
        deleteNotesForUser(select(note for note in ArchivedNote if note.userId==userId))
        deleteNotesForUser(select(note for note in DeletedNote if note.userId==userId))

        commit()

    return {}

@app.post("/api/logout")
def logout():
    data = request.get_data()
    userData = json.loads(data)
    if userData['userId'] in activeUsers:
        del activeUsers[userData['userId']]
    return {}

@app.post("/api/image")
def upload():
    for name in request.files:
        userId = int(name[4:])
        if (not isSesssionIdOK(request, userId)): return {'resultOk': False}
        file = request.files[name]
        with db_session:
            resp = select(user for user in User if user.id==userId)
            for u in resp:
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], name+'_'+str(u.image+1)))
                try: os.remove(os.path.join(app.config['UPLOAD_FOLDER'], name+'_'+str(u.image)))
                except: pass
                u.set(image=u.image+1, defaultImage=-1)
                commit()
                return {'resultOK': True, 'image': u.image}
    return {'resultOK': False}

@app.get("/api/check")
def check():
    return {'result': True}
