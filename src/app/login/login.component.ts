import { Component } from '@angular/core';
import { LoginData }  from '../user'
import { NotesService } from '../notes.service';
import { BackgroundService } from '../background.service';
import { Router } from '@angular/router'

import { Observable, of } from 'rxjs';

const shaJS = require('sha.js')

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  host: {
    '(window:resize)':  'fixBackground()'
  }
})
export class LoginComponent {

	constructor(private notesService: NotesService, private router: Router, private backgroundService: BackgroundService) 
	{
		if (notesService.logged) 
		{
			this.router.navigateByUrl('notes');
		}
		this.notesService.checkServer().subscribe((data)=>{
			console.log("check Server done");
			console.log("result: " + data.result);
			this.serverEnabled = data.result;
		});
		this.backgroundService.setImage(0, 0, 0);
	}
	
	serverEnabled: boolean = false;
	createMode: boolean = false;
	badLoginData: boolean = false;
	errorInfo: string = "";

	user: string = "";
	password1: string = "";
	password2: string = "";

	setCreateMode(value: boolean)
	{
		this.createMode = value;
		this.badLoginData = false;
		this.errorInfo = "";
	}
	
	goLocal()
	{
		this.notesService.setLocalService();
		this.notesService.setLogged(0, 0);
		this.router.navigateByUrl('notes');
	}

	login()
	{
		this.notesService.setHttpService();
		var hash = shaJS('sha256').update(this.password1).digest('hex');
		this.notesService.login({name: this.user, password: hash}).subscribe((data)=>{
			console.log(data);
			if (!data['login']) 
			{
				this.badLoginData = true;
				this.errorInfo = data['error'];
				return;
			}
			this.badLoginData = false;
			this.notesService.setLogged(data['userId'], data['sessionId']);
			this.router.navigateByUrl('notes');
		});
	}

	createUser()
	{
		this.notesService.setHttpService();
		var hash = shaJS('sha256').update(this.password1).digest('hex');
		this.notesService.createUser({name: this.user, password: hash}).subscribe((data)=>{
			console.log(data);
			if (!data['login']) 
			{
				this.badLoginData = true;
				this.errorInfo = data['error'];
				return;
			}
			this.badLoginData = false;
			this.notesService.setLogged(data['userId'], data['sessionId']);
			this.router.navigateByUrl('notes');
		});
	}

	fixBackground()
	{
		this.backgroundService.fixRatio();
	}

}
