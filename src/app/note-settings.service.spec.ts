import { TestBed } from '@angular/core/testing';

import { NoteSettingsService } from './note-settings.service';

describe('NoteSettingsService', () => {
  let service: NoteSettingsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NoteSettingsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
