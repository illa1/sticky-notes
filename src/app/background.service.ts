import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BackgroundService
{

	image: number = 0;
	userId: number = 0;
	selectedDefault: number = 0;

	defaultImages: Array<number> = [0, 1, 2, 3, 4, 5, 6];
	backgroundRatio: number = 0;

	setImage(defaultImage: number, image: number, userId: number)
	{
		this.selectedDefault = defaultImage;
		this.image = image;
		this.userId = userId;
		this.setBackground();
	}

	fixRatio()
	{
		var bodyEl = document.getElementById('backgroundElement');
		if (!bodyEl) return;

		var screenRatio = window.innerWidth/window.innerHeight;
		if (screenRatio < this.backgroundRatio)
		{
			bodyEl.style.backgroundSize = "auto 100%";
		}
		else
		{
			bodyEl.style.backgroundSize = "100% auto";
		}
	}

	private isBackgroundFromUser()
	{
		return this.selectedDefault == -1;
	}

	private setBackground()
	{
		var path = "./assets/";
		var name = "";
		if (this.isBackgroundFromUser())
		{
			name = 'user'+this.userId+'_'+this.image;
		}
		else
		{
			name = 'default_'+this.selectedDefault+'.jpg'
		}

		var imageSize = new Image();
		imageSize.src = path+name;
		var t = this;

		imageSize.onload = function () {
			t.backgroundRatio = imageSize.naturalWidth/imageSize.naturalHeight;
			console.log(t.backgroundRatio);
			t.fixRatio();
		}

		var bodyEl = document.getElementById('backgroundElement');
		if (bodyEl)
		{
			bodyEl.style.backgroundImage = "url('" + path+name + "')";
		}
		this.fixRatio();
	}
}
