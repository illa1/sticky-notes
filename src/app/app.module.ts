import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AngularResizeEventModule } from 'angular-resize-event';
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NotesComponent } from './notes/notes.component';
import { NotesArchivedComponent } from './notes-archived/notes-archived.component';
import { LoginComponent } from './login/login.component';
import { NotesDeletedComponent } from './notes-deleted/notes-deleted.component';
import { NotesCommonComponent } from './notes-common/notes-common.component';
import { SettingsComponent } from './settings/settings.component';
import { GlobalSettingsComponent } from './global-settings/global-settings.component';

@NgModule({
  declarations: [
    AppComponent,
    NotesComponent,
    NotesArchivedComponent,
    LoginComponent,
    NotesDeletedComponent,
    NotesCommonComponent,
    SettingsComponent,
    GlobalSettingsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    AngularResizeEventModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
