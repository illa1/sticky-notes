import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Note } from './note';
import { LoginData, SettingsData } from './user';

export class HttpClientHelper {

    static baseURL: string = 'http://127.0.0.1:5000/';
}

@Injectable({
  providedIn: 'root'
})
export class HttpService {

    constructor(private http: HttpClient) { }

	private userId: number = 0;

	private notesUrl = 'api/notes';
	private archivedUrl = 'api/archived';
	private deletedUrl = 'api/deleted';
	private usersUrl = 'api/users';
	private checkUrl = 'api/check';

	private sessionId: number = 0;

	httpOptions =
	{
	  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
	};

	getNotes(): Observable<Note[]> {
		return this.http.get<Note[]>(`${HttpClientHelper.baseURL}`+this.notesUrl+'?userId='+this.userId, this.httpOptions).pipe(
				tap(_ => console.log('fetched notes ok')),
				catchError(this.handleError<Note[]>('getNotes', []))
      );
	}

	private handleError<T>(operation = 'operation', result?: T)
	{
		return (error: any): Observable<T> => {
			return of(result as T);
		};
	}

	getArchivedNotes(): Observable<Note[]> {
		return this.http.get<Note[]>(`${HttpClientHelper.baseURL}`+this.archivedUrl+'?userId='+this.userId, this.httpOptions)
			.pipe(
				tap(_ => console.log('fetched notes ok')),
				catchError(this.handleError<Note[]>('getArchivedNotes', []))				
        );
	}

	getDeletedNotes(): Observable<Note[]> {
		return this.http.get<Note[]>(`${HttpClientHelper.baseURL}`+this.deletedUrl+'?userId='+this.userId, this.httpOptions)
			.pipe(
				tap(_ => console.log('fetched notes ok')),
				catchError(this.handleError<Note[]>('getDeletedNotes', []))				
      );
	}

	updateNote(note: Note): Observable<any>
	{
		return this.http.put(`${HttpClientHelper.baseURL}`+this.notesUrl, note, this.httpOptions).pipe(
			tap(_ => console.log(`updated note`)),
			catchError(this.handleError<any>('updateNote'))
		);
	}

	removeFromArchived(id: number): Observable<any>
    {
		return this.http.delete<Note>(`${HttpClientHelper.baseURL}`+this.archivedUrl+"/delete?id="+id, this.httpOptions).pipe(
			tap(() => console.log('remove From Archived')),
			catchError(this.handleError<Note>('removeFromArchived'))
		);
	}

	removeFromDeleted(id: number): Observable<any>
    {
		return this.http.delete<Note>(`${HttpClientHelper.baseURL}`+this.deletedUrl+"/delete?id="+id, this.httpOptions).pipe(
			tap(() => console.log('remove From Deleted')),
			catchError(this.handleError<Note>('removeFromDeleted'))
		);
	}

	removeFromNotes(id: number): Observable<any>
    {
		return this.http.delete<Note>(`${HttpClientHelper.baseURL}`+this.notesUrl+"/delete?id="+id, this.httpOptions).pipe(
			tap(() => console.log('remove from notes')),
			catchError(this.handleError<Note>('removeFromNotes'))
		);
	}

   addToNotes(note: Note): Observable<Note>
   {
      return this.http.post<Note>(`${HttpClientHelper.baseURL}`+this.notesUrl, note, this.httpOptions).pipe(
		tap((newNote: Note) => console.log('added new Note')),
		catchError(this.handleError<Note>('addNote'))
      );
   }

	addToDeleted(note: Note): Observable<Note>
    {
		return this.http.post<Note>(`${HttpClientHelper.baseURL}`+this.deletedUrl, note, this.httpOptions).pipe(
			tap((newNote: Note) => console.log('added to deleted')),
			catchError(this.handleError<Note>('addToDeleted'))
		);
	}

	addToArchived(note: Note): Observable<Note>
    {
		return this.http.post<Note>(`${HttpClientHelper.baseURL}`+this.archivedUrl, note, this.httpOptions).pipe(
			tap((newNote: Note) => console.log('added to archived')),
			catchError(this.handleError<Note>('addToArchived'))
		);
	}

	login(loginData: LoginData): Observable<any> {
		return this.http.post<any>(`${HttpClientHelper.baseURL}`+'api/login', loginData)
			.pipe(
				tap(_ => {},
				catchError(this.handleError<any>('login', []))
      ));
	}

	createUser(loginData: LoginData): Observable<any> {
		return this.http.post<any>(`${HttpClientHelper.baseURL}`+'api/create', loginData)
			.pipe(
				tap(_ => {},
				catchError(this.handleError<any>('create', []))
      ));
	}

	getSettings() : Observable<any>{
		return this.http.get<any>(`${HttpClientHelper.baseURL}`+'api/settings?userId='+this.userId, this.httpOptions)
			.pipe(
				tap(_ => {},
				catchError(this.handleError<any>('settings', []))
      ));
	}

	uploadImage(image: FormData) : Observable<any>
	{
		var options2 = this.httpOptions;
		options2.headers = options2.headers.delete('Content-Type');
		console.log("uploading image");
		return this.http.post<any>(`${HttpClientHelper.baseURL}`+'api/image', image, options2)
			.pipe(
				tap(_ => {},
				catchError(this.handleError<any>('settings', []))
      ));	
	}

	updateSettings(settingsData: SettingsData) : Observable<any>{
		return this.http.post<any>(`${HttpClientHelper.baseURL}`+'api/settings', settingsData, this.httpOptions)
			.pipe(
				tap(_ => {},
				catchError(this.handleError<any>('settings', []))
      ));
	}

	updatePassword(passwordHash: string) : Observable<any>{
		return this.http.post<any>(`${HttpClientHelper.baseURL}`+'api/password', {userId: this.userId, password: passwordHash}, this.httpOptions)
			.pipe(
				tap(_ => {},
				catchError(this.handleError<any>('settings', []))
      ));
	}

	setLogged(userId: number, sessionId: number)
	{
		this.userId = userId;
		this.sessionId = sessionId;
		this.httpOptions.headers = this.httpOptions.headers.set("Sessionid", ""+this.sessionId);
	}

	logout(): Observable<any> {
		return this.http.post<any>(`${HttpClientHelper.baseURL}`+'api/logout', {userId:this.userId})
			.pipe(
				tap(_ => {},
				catchError(this.handleError<any>('logout', []))
      ));
	}

	removeUser(): Observable<any>
    {
		return this.http.delete<any>(`${HttpClientHelper.baseURL}`+this.usersUrl+"/delete?id="+this.userId, this.httpOptions).pipe(
			tap(() => console.log('remove from users')),
			catchError(this.handleError<any>('removeUser')));
	}

	check(): Observable<any>
	{
		return this.http.get<any>(`${HttpClientHelper.baseURL}`+"api/check").pipe(
			tap(() => console.log('check')),
			catchError(()=>{
				return of({result: false});
			}));
	}
}
