import { Component } from '@angular/core';
import { Note } from '../note'
import { NotesService } from '../notes.service';
import { Router } from '@angular/router'
import { NotesCommonComponent} from '../notes-common/notes-common.component';
import { BackgroundService } from '../background.service';

@Component({
  selector: 'app-notes-archived',
  templateUrl: './notes-archived.component.html',
  styleUrls: ['./notes-archived.component.css'],
  host: {
    '(window:resize)':  'fixBackground()'
  }
})

export class NotesArchivedComponent extends NotesCommonComponent {
	
    constructor(public override notesService: NotesService,
		public override router: Router,
		private backgroundService: BackgroundService) 
    {
		super(notesService, router);
    }
	
	override restoreNote(note: Note)
	{
		var id = note.id;
		this.notesService.addToNotes(note).subscribe((note)=>{this.notes.push(note);});
		this.notesService.removeFromArchived(id).subscribe(()=>{this.deleteById(id, this.archived)});
	}

	override deleteNote(note: Note)
	{
		this.notesService.addToDeleted(note).subscribe((note)=>{this.deleted.push(note);});
		this.notesService.removeFromArchived(note.id).subscribe(()=>{this.deleteById(note.id, this.archived)});
	}

	sortByNameMode: number = 1;
	sortByDateMode: number = 1;
	
	sortByName()
	{
		this.sortByDateMode = 1;
		var sortByNameMode = this.sortByNameMode;
		this.archived.sort(function compareFn(a, b)
		{ 
			if (a.title == b.title) return 0; 
			if (a.title < b.title) return -1*sortByNameMode;
			return 1*sortByNameMode;
		});
		this.sortByNameMode = -this.sortByNameMode;
	}

	sortByDate()
	{
		this.sortByNameMode = 1;
		var sortByDateMode = this.sortByDateMode;
		this.archived.sort(function compareFn(a, b)
		{ 
			if (a.id == b.id) return 0; 
			if (a.id < b.id) return -1*sortByDateMode;
			return 1*sortByDateMode;
		});	
		this.sortByDateMode = -this.sortByDateMode;
	}

	getOriginalOrder()
	{
		this.notesService.getArchivedNotes().subscribe(archived => this.archived = archived);
	}

	fixBackground()
	{
		this.backgroundService.fixRatio();
	}
}
