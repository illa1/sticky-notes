import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotesArchivedComponent } from './notes-archived.component';

describe('NotesArchivedComponent', () => {
  let component: NotesArchivedComponent;
  let fixture: ComponentFixture<NotesArchivedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotesArchivedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NotesArchivedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
