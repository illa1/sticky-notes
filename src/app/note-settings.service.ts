import { Injectable } from '@angular/core';
import { SettingsComponent} from './settings/settings.component';

@Injectable({
  providedIn: 'root'
})
export class NoteSettingsService {

  constructor() { }
  
  noteSettings!: SettingsComponent;

  set(settings: SettingsComponent)
  {
    this.noteSettings = settings;
  }

  get()
  {
    return this.noteSettings;
  }
}
