import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotesDeletedComponent } from './notes-deleted.component';

describe('NotesDeletedComponent', () => {
  let component: NotesDeletedComponent;
  let fixture: ComponentFixture<NotesDeletedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotesDeletedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NotesDeletedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
