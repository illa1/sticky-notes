import { Component } from '@angular/core';
import { Note } from '../note'
import { NotesService } from '../notes.service';
import { Router } from '@angular/router'
import { NotesCommonComponent} from '../notes-common/notes-common.component';
import { BackgroundService } from '../background.service';


@Component({
  selector: 'app-notes-deleted',
  templateUrl: './notes-deleted.component.html',
  styleUrls: ['./notes-deleted.component.css'],
  host: {
    '(window:resize)':  'fixBackground()'
  }
})
export class NotesDeletedComponent extends NotesCommonComponent {

	constructor(public override notesService: NotesService,
		public override router: Router,
		private backgroundService: BackgroundService) 
	{
		super(notesService, router);
	}
	
	override restoreNote(note: Note)
	{
		var id = note.id;
		this.notesService.addToNotes(note).subscribe((note)=>{this.notes.push(note);});
		this.notesService.removeFromDeleted(id).subscribe(()=>{this.deleteById(id, this.deleted)});
	}

	override archiveNote(note: Note)
	{
		this.notesService.addToArchived(note).subscribe((note)=>{this.archived.push(note);});
		this.notesService.removeFromDeleted(note.id).subscribe(()=>{this.deleteById(note.id, this.deleted)});
	}

	override deleteNote(note: Note)
	{
		var id = note.id
		this.notesService.removeFromDeleted(id).subscribe(()=>{this.deleteById(id, this.deleted)});
	}

	removeNext(t: any, idsToRemove: number[], current: number)
	{
		t.deleteById(idsToRemove[current], t.deleted);

		if (current+1 == idsToRemove.length) return; 
		t.notesService.removeFromDeleted(idsToRemove[current+1]).subscribe(()=>{t.removeNext(t, idsToRemove, current+1);});
	}

	removeAllVisible()
	{
		var idsToRemove: number[] = [];
		for (var i = 0; i < this.deleted.length; i++)
		{
			if (this.deleted[i].hidden == false)
			{
				idsToRemove.push(this.deleted[i].id);
			}
		}
		if (idsToRemove.length > 0)
		{
			this.notesService.removeFromDeleted(idsToRemove[0]).subscribe(()=>{this.removeNext(this, idsToRemove, 0)});
		}
	}

	fixBackground()
	{
		this.backgroundService.fixRatio();
	}
}
