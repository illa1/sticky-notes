import { TestBed } from '@angular/core/testing';

import { NoteUpdateService } from './note-update.service';

describe('NoteUpdateService', () => {
  let service: NoteUpdateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NoteUpdateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
