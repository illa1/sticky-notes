import { Component } from '@angular/core';
import { Note, Palette } from '../note'
import { ResizedEvent } from 'angular-resize-event';
import { NotesService } from '../notes.service';
import { GlobalSettingsService } from '../global-settings.service';
import { NoteSettingsService } from '../note-settings.service';
import { NoteUpdateService } from '../note-update.service';
import { NotesCommonComponent} from '../notes-common/notes-common.component';
import { Router } from '@angular/router'

class Size
{
	x: number = 0;
	y: number = 0;
	width: number = 0;
	height: number = 0;
}

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent extends NotesCommonComponent {

	// from other file
	titleFonts: Array<number> = [22, 26, 30, 34]; 
	textFonts: Array<number> = [14, 18, 20, 24];

    constructor(public override notesService: NotesService, 
		public override router: Router,
		public globalSettings: GlobalSettingsService,
		public noteSettings: NoteSettingsService,
		public noteUpdateService: NoteUpdateService)
    {
		super(notesService, router);
    }

	override ngOnInit()
	{
		this.userId = this.notesService.userId;
		this.notesService.getNotes().subscribe((notes) => {this.notes = notes; this.minimizeZ();});
		this.notesService.getArchivedNotes().subscribe((archived) => {this.archived = archived;});
		this.notesService.getDeletedNotes().subscribe((deleted) => {this.deleted = deleted;});
	}

	pageX : number = 0;
	pageY : number = 0;

	addNote()
	{
		//  z ?
		var colors = this.globalSettings.get().getColors();
		var fontSize = this.globalSettings.get().getFontSize();

		var newNote =
		{
			id: -1,
			x: 400, y: 400, z: this.findZMax(), width: 250, height: 250,
			title: "Note", text:"Text ...", 
			color: colors[this.notes.length%colors.length], fontSize: fontSize, hidden: false,
			userId: this.userId
		};

		this.notesService.addToNotes(newNote as Note)
		  .subscribe(note => {
			newNote.id = note.id;
			this.notes.push(newNote);
			setTimeout(function(){
					var el = document.getElementById('TitleInput'+newNote.id);
					if (el) el.focus();
				},500);
		});
	}

	areOverlapping(note1: Size, note2: Size)
	{
		var okOnX = note1.x > note2.x+note2.width || note2.x > note1.x+note1.width;
		var okOnY = note1.y > note2.y+note2.height || note2.y > note1.y+note1.height;

		return !(okOnX || okOnY);
	}

	bringToFront(note: Note)
	{
		var noteEl = document.getElementById('Note'+note.id);
		if (!noteEl) return;
		
		var noteSize: Size = {x: note.x, y: note.y, width: noteEl.offsetWidth, height: noteEl.offsetHeight};

		for (let n of this.notes)
		{
			if (n.id == note.id) continue;

			var el = document.getElementById('Note'+n.id);
			if (!el) continue;
			var nSize: Size = {x: n.x, y: n.y, width: el.offsetWidth, height: el.offsetHeight};
			
			if (this.areOverlapping(noteSize, nSize))
			{
				if (note.z <= n.z) note.z = n.z+1;
			}
		}
	}

	minimizeZ()
	{
		this.notes.sort((note1: Note, note2: Note)=>{
			if (note1.z == note2.z) return 0;
			if (note1.z < note2.z) return -1;
			return 1;
		});

		for (var i = 0; i < this.notes.length; i++)
		{
			this.notes[i].z = i + 1;
			this.noteUpdateService.update(this.notes[i]);
		}
	}

	override dragStart(id: number, $event: any): void
	{
		super.dragStart(id, $event);
		this.pageX = $event.pageX;
		this.pageY = $event.pageY;
	}

	override dragEnd(note: Note, $event: any): void
	{
		if (this.overZoneName == "")
		{
			var deltaX = $event.pageX - this.pageX;
			var deltaY = $event.pageY - this.pageY;
			note.x += deltaX;
			note.y += deltaY;
			this.bringToFront(note);
			this.noteUpdateService.update(note);
		}
		this.pageX = 0;
		this.pageY = 0;

		super.dragEnd(note, $event);
	}

	doubleClick(note: Note)
	{
		this.bringToFront(note);
		this.noteUpdateService.update(note);
	}

	saveSize(note: Note, $event: ResizedEvent): void
	{
		if (!$event.oldRect) return;
		if ($event.newRect.width < 30 || $event.newRect.height < 30) return;

		var textarea = document.getElementById("TextInput"+note.id);
		if (!textarea) return;

		if (textarea.scrollHeight > textarea.clientHeight)
		{
			var scrollWidth = textarea.offsetWidth-textarea.clientWidth
			note.width = $event.newRect.width + scrollWidth;
		}
		else
		{
			note.width = $event.newRect.width;
		}
		note.height = $event.newRect.height;

		this.noteUpdateService.update(note);
	}

	saveText(note: Note)
	{
		this.noteUpdateService.update(note);
	}

    logout()
    {
		this.notesService.logout().subscribe((data)=>{
			console.log(data);
			this.notesService.setLogout();
			this.globalSettings.get().setDefaultBackground();
			this.goToPage('login');
		});
    }
    
    override goToPage(name: string)
    {
		this.noteUpdateService.updateAll();
		super.goToPage(name);
    }
    
	override deleteNote(note: Note)
	{
		this.noteUpdateService.updateAll();
		this.notesService.addToDeleted(note).subscribe((note)=>{this.deleted.push(note)});
		this.notesService.removeFromNotes(note.id).subscribe(()=>this.deleteById(note.id, this.notes));
	}

	override archiveNote(note: Note)
	{
		this.noteUpdateService.updateAll();
		this.notesService.addToArchived(note).subscribe((note)=>{this.archived.push(note)});
		this.notesService.removeFromNotes(note.id).subscribe(()=>this.deleteById(note.id, this.notes));
	}

	private findZMax()
	{
		var zMax: number = 1;
		for (let note of this.notes)
		{
			if (note.z > zMax) zMax = note.z;
		}
		return zMax;
	}

	showGlobalSettings()
	{
		var zMax = this.findZMax()+1;
		this.globalSettings.get().setVisible(zMax);
	}

	showNoteSettings(note: Note)
	{
		var zMax = this.findZMax();
		this.noteSettings.get().setVisible(note, zMax);
	}
}
