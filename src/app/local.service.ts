import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Note } from './note';
import { SettingsData } from './user';


@Injectable({
  providedIn: 'root'
})
export class LocalService {

	initIfEmpty()
	{
		//localStorage.clear();

		var defaultSettings =
		{
			palette: 0,
			fontSize: 1,
			image: 0,
			defaultImage: 0
		};

		if (!localStorage.getItem("notes")) localStorage.setItem("notes", "[]");
		if (!localStorage.getItem("archived")) localStorage.setItem("archived", "[]");
		if (!localStorage.getItem("deleted")) localStorage.setItem("deleted", "[]");
		if (!localStorage.getItem("settings")) localStorage.setItem("settings", JSON.stringify(defaultSettings));
		if (!localStorage.getItem("maxId")) localStorage.setItem("maxId", "0");
	}

	logged: boolean = false;

	getNotes(): Observable<Note[]>
	{
		var notesStr = localStorage.getItem("notes");
		if (notesStr) return of(JSON.parse(notesStr));
		return of([]);
	}

	getArchivedNotes(): Observable<Note[]> {
		var notesStr = localStorage.getItem("archived");
		if (notesStr) return of(JSON.parse(notesStr));
		return of([]);
	}

	getDeletedNotes(): Observable<Note[]> {
		var notesStr = localStorage.getItem("deleted");
		if (notesStr) return of(JSON.parse(notesStr));
		return of([]);
	}

	updateNote(note: Note): Observable<any>
	{
		var notesStr = localStorage.getItem("notes");
		var notes = [];
		if (notesStr) notes = JSON.parse(notesStr);
		for (var i = 0; i < notes.length; i++)
		{
			if (notes[i].id == note.id)
			{
				notes[i] = note;
				localStorage.setItem("notes", JSON.stringify(notes));
				return of(note);
			}
		}
		return of({});
	}

	private removeById(listName: string, id:number)
	{
		var notesStr = localStorage.getItem(listName);
		var notes = [];
		if (notesStr) notes = JSON.parse(notesStr);
		for (var i = 0; i < notes.length; i++)
		{
			if (notes[i].id == id)
			{
				notes.splice(i, 1);
				localStorage.setItem(listName, JSON.stringify(notes));
				return;
			}
		}
	}

	removeFromArchived(id: number): Observable<any>
    {
		this.removeById("archived", id);
		return of({});
	}

	removeFromDeleted(id: number): Observable<any>
    {
		this.removeById("deleted", id);
		return of({});
	}

	removeFromNotes(id: number): Observable<any>
    {
		this.removeById("notes", id);
		return of({});
	}

	private addToList(listName: string, note: Note)
	{
		var notesStr = localStorage.getItem(listName);
		var notes = [];
		if (notesStr) notes = JSON.parse(notesStr);

		notes.push(note);
		localStorage.setItem(listName, JSON.stringify(notes));
	}

    addToNotes(note: Note): Observable<Note>
    {
		var maxIdStr = localStorage.getItem("maxId");
		var maxId = 0;
		if (maxIdStr) maxId = JSON.parse(maxIdStr);
		
		var noteCp = {...note};
		noteCp.id = maxId+1;
		localStorage.setItem("maxId", JSON.stringify(maxId+1));
		
		this.addToList("notes", noteCp);

		return of(noteCp);
    }

	addToDeleted(note: Note): Observable<Note>
    {
		this.addToList("deleted", note);
		return of(note);
	}

	addToArchived(note: Note): Observable<Note>
    {
		this.addToList("archived", note);
		return of(note);
	}

	getSettings() : Observable<any>
	{
		var settingsStr = localStorage.getItem("settings");
		var settings = {};
		if (settingsStr) settings = JSON.parse(settingsStr);

		return of(settings);
	}

	updateSettings(settingsData: SettingsData) : Observable<any>
	{
		localStorage.setItem("settings", JSON.stringify(settingsData));
		return of({});
	}

	private setMaxId(notes: Note[])
	{
		var maxIdStr = localStorage.getItem("maxId");
		var maxId = 0;
		if (maxIdStr) maxId = JSON.parse(maxIdStr);

		for (var i = 0; i < notes.length; i++)
		{
			if (notes[i].id > maxId) maxId = notes[i].id;
		}
		localStorage.setItem("maxId", JSON.stringify(maxId));
	}
	
	setAll(notes : Note[], deleted: Note[], archived: Note[], settings: SettingsData)
	{
		localStorage.clear();
		localStorage.setItem("maxId", "0");
		localStorage.setItem("notes", JSON.stringify(notes));
		localStorage.setItem("deleted", JSON.stringify(deleted));
		localStorage.setItem("archived", JSON.stringify(archived));
		localStorage.setItem("settings", JSON.stringify(settings));
		this.setMaxId(notes);
		this.setMaxId(deleted);
		this.setMaxId(archived);
	}
}
