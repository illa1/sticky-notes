import { Injectable } from '@angular/core';
import { GlobalSettingsComponent} from './global-settings/global-settings.component';

@Injectable({
  providedIn: 'root'
})
export class GlobalSettingsService {

  constructor() { }
  
  globalSettings!: GlobalSettingsComponent;

  set(settings: GlobalSettingsComponent)
  {
    this.globalSettings = settings;
  }

  get()
  {
    return this.globalSettings;
  }
}
