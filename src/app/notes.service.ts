import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { LocalService } from './local.service';
import { HttpService } from './http.service';

import { Note } from './note';
import { LoginData, SettingsData } from './user';

@Injectable({
  providedIn: 'root'
})
export class NotesService
{
	public userId: number = 0; // change!
	public logged: boolean = false; // change!

	private mode: string = "http";

    constructor(private localService: LocalService, private httpService: HttpService) { }
	
	setHttpService()
	{
		this.mode = "http";
	}

	setLocalService()
	{
		this.localService.initIfEmpty();
		this.mode = "local";
	}

	private get()
	{
		if (this.mode == "local") return this.localService;
		return this.httpService;
	}

	getNotes(): Observable<Note[]>
	{
		return this.get().getNotes();
	}

	getArchivedNotes(): Observable<Note[]>
	{
		return this.get().getArchivedNotes();
	}

	getDeletedNotes(): Observable<Note[]>
	{
		return this.get().getDeletedNotes();
	}

	updateNote(note: Note): Observable<any>
	{
		return this.get().updateNote(note);
	}

	removeFromArchived(id: number): Observable<any>
    {
		return this.get().removeFromArchived(id);
	}

	removeFromDeleted(id: number): Observable<any>
    {
		return this.get().removeFromDeleted(id);
	}

	removeFromNotes(id: number): Observable<any>
    {
		return this.get().removeFromNotes(id);
	}

	addToNotes(note: Note): Observable<Note>
	{
		return this.get().addToNotes(note);
	}

	addToDeleted(note: Note): Observable<Note>
    {
		return this.get().addToDeleted(note);
	}

	addToArchived(note: Note): Observable<Note>
    {
		return this.get().addToArchived(note);
	}

	login(loginData: LoginData): Observable<any>
	{
		if (this.mode == "http") return this.httpService.login(loginData);
		return of({})
	}

	createUser(loginData: LoginData): Observable<any>
	{
		if (this.mode == "http") return this.httpService.createUser(loginData);
		return of({})
	}

	getSettings() : Observable<any>
	{
		return this.get().getSettings();
	}

	uploadImage(image: FormData) : Observable<any>
	{
		if (this.mode == "http") return this.httpService.uploadImage(image);
		return of({})
	}

	updateSettings(settingsData: SettingsData) : Observable<any>
	{
		return this.get().updateSettings(settingsData);
	}

	updatePassword(passwordHash: string) : Observable<any>
	{
		if (this.mode == "http") return this.httpService.updatePassword(passwordHash);
		return of({});
	}

	setLogged(userId: number, sessionId: number)
	{
		if (this.mode == "http") this.httpService.setLogged(userId, sessionId);
		this.logged = true;
		this.userId = userId;
	}

	logout(): Observable<any>
	{
		if (this.mode == "http") return this.httpService.logout();
		return of({});
	}

	setLogout()
	{
		this.logged = false;
		this.userId = 0;
	}

	removeUser(): Observable<any>
    {
		if (this.mode == "http") return this.httpService.removeUser();
		return of({})
	}
	
	saveToLocal()
	{
		if (this.mode == "http")
		{
			var notes!: Note[];
			var deleted!: Note[];
			var archived!: Note[];
			var settings! : SettingsData;

			var waitForAll = ()=>
			{
				if (!notes || !deleted || !archived || !settings) return;
				this.localService.setAll(notes, deleted, archived, settings);
			};

			this.httpService.getNotes().subscribe((data)=>{notes = data; waitForAll();});
			this.httpService.getDeletedNotes().subscribe((data)=>{deleted = data; waitForAll();});
			this.httpService.getArchivedNotes().subscribe((data)=>{archived = data; waitForAll();});
			this.httpService.getSettings().subscribe((data)=>{settings = data; waitForAll();});
		}
	}
	isLocalMode()
	{
		return this.mode == "local";
	}
	
	checkServer() : Observable<any>
	{
		return this.httpService.check();
	}
}
