import { Component } from '@angular/core';
import { NotesService } from '../notes.service';
import { Note, Palette } from '../note'
import { GlobalSettingsService } from '../global-settings.service';
import { Router } from '@angular/router'
import { BackgroundService } from '../background.service';

const shaJS = require('sha.js')

@Component({
  selector: 'app-global-settings',
  templateUrl: './global-settings.component.html',
  styleUrls: ['./global-settings.component.css'],
  host: {
    '(window:resize)': 'fixBackground()'
  }
})
export class GlobalSettingsComponent {

	visible: boolean = false;
	inLocalMode: boolean = false;

	// move to some JSON ?
	palette1: Array<string> = ["#ffb3ba", "#ffdfba", "#ffffba","#e1ffba", "#baffc9", "#bae1ff", "#babfff", "#D8BFD8"];  // pastel
	palette2: Array<string> = ["#ff0000", "#ffac00", "#fff100", "#0bff00", "#00f6ff"];  // neon
	palette3: Array<string> = ["#da92d0", "#e1a4d8", "#e7b8e0", "#edcbe8", "#f3ddf0"]; // mono pink
	palette4: Array<string> = ["#d3f0e3", "#89c4c7", "#84ced1", "#90e8db"]; // sea green
	palettes: Array<Palette> = [
		{colors: this.palette1, width: 100.0/this.palette1.length},
		{colors: this.palette2, width: 100.0/this.palette2.length},
		{colors: this.palette3, width: 100.0/this.palette3.length},
		{colors: this.palette4, width: 100.0/this.palette4.length},
	];


	selectedPalette: number = 0;
	selectedFontSize: number = 0;

	selectedDefault: number = 0;
	image: number = 0;

	defaultImages: Array<number> = [0, 1, 2, 3, 4, 5, 6];

	zIndex: number = 1;

	userId: number = -1; 
	constructor(
		public router: Router,
		public notesService: NotesService,
		public settingsService: GlobalSettingsService,
		private backgroundService: BackgroundService)
	{
	}
	
	ngOnInit()
	{
		this.userId = this.notesService.userId;
		this.inLocalMode = this.notesService.isLocalMode();
		this.notesService.getSettings().subscribe((settings)=>{
			this.selectedPalette = settings.palette;
			this.selectedFontSize = settings.fontSize;
			this.image = settings.image;
			this.selectedDefault = settings.defaultImage;
			this.backgroundService.setImage(settings.defaultImage, settings.image, this.userId);
		});
		this.settingsService.set(this);
	}

	setVisible(zIndex: number)
	{
		this.zIndex = zIndex;
		this.showPreview = false;
		this.visible = true;
	}

	getColors()
	{
		return this.palettes[this.selectedPalette].colors;
	}
	
	getFontSize()
	{
		return this.selectedFontSize;
	}

	setPalette(palette: number)
	{
		this.selectedPalette = palette;
		this.saveSettings();
	}

	setGlobalFontSize(size: number)
	{
		this.selectedFontSize = size;
		this.saveSettings();
	}

	setDefaultImage(image: number)
	{
		this.selectedDefault = image;
		this.backgroundService.setImage(this.selectedDefault, this.image, this.userId);
		this.saveSettings();
	}

	file!: File;
	fileValid: boolean = false;
	showPreview: boolean = false;

	tryPreview($event: any)
	{
		if ($event.srcElement.value == "")
		{
			this.showPreview = false;
			return;
		}
		var [file] = $event.srcElement.files;
		this.file = file;
		this.fileValid = true;
		this.showPreview = true;
		var previewImg = document.getElementById('previewImg') as HTMLImageElement;
		if (previewImg)
		{
			previewImg.src = URL.createObjectURL(file);
		}
	}
	
	uploadImage($event: any)
	{
	   if (this.file && this.fileValid)
		{
			const formData = new FormData();
			formData.append('user'+this.userId, this.file);
			this.notesService.uploadImage(formData).subscribe((response)=>
			{
				this.selectedDefault = -1;
				this.image = response.image;
				this.backgroundService.setImage(this.selectedDefault, this.image, this.userId);
			});
		}
   }
	private isBackgroundDefault()
	{
		return this.image == 0 && this.selectedDefault == 0;
	}

	setDefaultBackground()
	{
		if (this.isBackgroundDefault()) return;

		this.image = 0;
		this.selectedDefault = 0;
		this.backgroundService.setImage(0, 0, this.userId);
	}

	saveSettings()
	{
		var settings = {
			userId: this.userId,
			palette: this.selectedPalette,
			fontSize: this.selectedFontSize,
			image: this.image,
			defaultImage: this.selectedDefault
		};
		this.notesService.updateSettings(settings).subscribe(()=>{});
	}

	// change to enum
	accountMode: string = "";
	PASSWORD: string = "password";
	LOCAL: string = "local";
	REMOVE: string = "remove";

	password1: string = "";
	password2: string = "";

	setAccountMode(mode: string)
	{
		this.accountMode = mode;
		this.password1 = "";
		this.password2 = "";
	}

	accountChange()
	{
		if (this.accountMode == this.PASSWORD)
		{
			var hash = shaJS('sha256').update(this.password1).digest('hex');
			this.notesService.updatePassword(hash).subscribe((data)=>{
				this.notesService.setLogged(this.userId, data['sessionId']);
				this.setAccountMode('');
			});
			return;
		}
		if (this.accountMode == this.LOCAL)
		{
			this.notesService.saveToLocal();
			return;
		}
		if (this.accountMode == this.REMOVE)
		{
			this.notesService.removeUser().subscribe(()=>{
				this.notesService.setLogout();
				this.router.navigateByUrl('login');
				});
		}
	}
	
	fixBackground()
	{
		this.backgroundService.fixRatio();
	}
}
