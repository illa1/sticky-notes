export interface LoginData
{
	name: string;
	password: string;
}

export interface SettingsData
{
	userId: number;
	palette: number;
	fontSize: number;
	image: number;
	defaultImage: number;
}
