import { Component } from '@angular/core';
import { Router } from '@angular/router'
import { NotesService } from '../notes.service';
import { Note } from '../note'

@Component({
  selector: 'app-notes-common',
  templateUrl: './notes-common.component.html',
  styleUrls: ['./notes-common.component.css']
})
export class NotesCommonComponent {

	notes: Array<Note> = [];
	deleted: Array<Note> = [];
	archived: Array<Note> = [];

	inDragMode: number = -1;
	overZoneName: string = "";

	pattern: string = "";

	userId: number = 0;

    constructor(public notesService: NotesService, public router: Router)
    {
		if (!notesService.logged) 
		{
			this.router.navigateByUrl('login');		
		}
    }

	ngOnInit() 
	{
		this.userId = this.notesService.userId;
		this.notesService.getNotes().subscribe(notes => this.notes = notes);
		this.notesService.getArchivedNotes().subscribe(archived => this.archived = archived);
		this.notesService.getDeletedNotes().subscribe(deleted => this.deleted = deleted);
	}

	dragStart(id: number, $event: any): void
	{
		this.inDragMode = id;
	}

	dragEnd(note: Note, $event: any): void
	{
		if (this.overZoneName == 'notes')
		{
			this.restoreNote(note);
		}
		if (this.overZoneName == 'deleted')
		{
			this.deleteNote(note);
		}
		if (this.overZoneName == "archived")
		{
			this.archiveNote(note);
		}

		this.inDragMode = -1;
		this.overZoneName = "";
	}

	enteredZone($event: any, zone: string)
	{
		$event.preventDefault();
		if (this.overZoneName == zone || this.inDragMode < 0) return;

		this.overZoneName = zone;		
	}

	exitedZone($event : any, zone: string)
	{
		if (this.overZoneName != zone || this.inDragMode < 0) return;
		
		this.overZoneName = "";
	}

	goToPage(page: string)
	{
		console.log("goTo: " + page)
		this.router.navigateByUrl(page);
	}
	
	setAllVisible(notes: Array<Note>)
	{
		this.pattern = "";

		for (let i = 0; i < notes.length; i++)
		{
			notes[i].hidden = false;
 		}
	}

	findNotes(notes: Array<Note>)
	{
		for (let i = 0; i < notes.length; i++)
		{
			notes[i].hidden = !(notes[i].title.includes(this.pattern) || notes[i].text.includes(this.pattern));
 		}	
	}

    deleteById(id: number, array: Note[])
    {
		for (var i = 0; i < array.length; i++)
		{
			if (array[i].id == id)
			{
				array.splice(i, 1);
				break;
			}
		}
    }

	deleteNote(note: Note) {}
	archiveNote(note: Note) {}
	restoreNote(note: Note) {}
}
