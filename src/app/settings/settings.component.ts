import { Component } from '@angular/core';
import { Note, Palette } from '../note'
import { NoteSettingsService } from '../note-settings.service';
import { GlobalSettingsService } from '../global-settings.service';
import { NoteUpdateService } from '../note-update.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent
{
	pageX : number = 0;
	pageY : number = 0;
	settingsPosX : number = 0;
	settingsPosY : number = 0;
	zIndex: number = 1;
	note!: Note;
	visible: boolean = false;
	colors: Array<string> = [];

	constructor(public noteUpdateService: NoteUpdateService,
		public settingsService: NoteSettingsService,
		public globalSettings: GlobalSettingsService)
	{
	}

	ngOnInit()
	{
		this.settingsService.set(this);
	}

	setVisible(note?: Note, zIndex?: number)
	{
		if (note == null || zIndex == null)
		{
			this.visible = false;
			return;
		}

		this.note = note;
		this.colors = this.globalSettings.get().getColors();

		this.visible = true;
		this.zIndex = zIndex;
		this.settingsPosX = note.x+100;
		this.settingsPosY = note.y+100;
	}

	setColor(color: string)
	{
		this.note.color = color;
		this.noteUpdateService.update(this.note);
	}

	setFontSize(size: number)
	{
		this.note.fontSize = size;
		this.noteUpdateService.update(this.note);
	}

	dragStartForSettings($event: any)
	{
		this.pageX = $event.pageX;
		this.pageY = $event.pageY;
	}

	dragEndForSettings($event: any)
	{
		var deltaX = $event.pageX - this.pageX;
		var deltaY = $event.pageY - this.pageY;
		this.settingsPosX += deltaX;
		this.settingsPosY += deltaY;
	}
}
