import { Injectable } from '@angular/core';
import { Note } from './note'
import { NotesService } from './notes.service';

@Injectable({
  providedIn: 'root'
})
export class NoteUpdateService
{

	waitingForChange: Map<number, Note> = new Map<number, Note>();
	changed: Set<number> = new Set<number>();

	constructor(private notesService: NotesService)
	{
		window.addEventListener('beforeunload', (event) => { this.updateAll(); });
	}

	update(note: Note)
	{
		var id = note.id;
		
		if (this.changed.has(id)) return;
	
		if (this.waitingForChange.has(id))
		{
			this.changed.add(id);
			return;
		}

		this.notesService.updateNote(note).subscribe(()=>{});
		this.waitingForChange.set(id, note);
		
		if (this.waitingForChange.size == 1) this.startUpdateTimer();
	}

	updateAll()
	{
		for (let note of this.waitingForChange.values())
		{
			if (this.changed.has(note.id))
			{
				this.notesService.updateNote(note).subscribe(()=>{});
			}
		}
		this.waitingForChange.clear();
		this.changed.clear();
	}

	private startUpdateTimer()
	{
		setTimeout(()=>{this.updateAll();}, 3000);
	}
}
