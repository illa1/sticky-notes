export interface Note {
  id: number;
  x: number;
  y: number;
  z: number;
  width: number;
  height: number;
  title: string;
  text: string;
  color: string;
  hidden: boolean;
  fontSize: number;
  userId: number;
}

export interface Palette {
  colors: Array<string>;
  width: number;
}
